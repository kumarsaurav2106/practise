package array;

public class MethodWithParameter {
	public static void main(String args[])
	{
		sayHello("saurav");
		sayHello("kumar");
		sayHello("Singhal");
		
	}
	public static void sayHello(String name ){
		System.out.println("Hi"+name);
	}
}
