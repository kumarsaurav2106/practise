package methodOverloading;

public class Method_OverLoading {

	public static void main(String[] args) {
		
		//overloading(10,20,30);
		//int d= overloading(10,20,30);
		System.out.println(overloading(10,20,30));
		//overloading(10.1,20.1,30.1);
		//double e=overloading(10.1, 20.1, 30.1);
		System.out.println(overloading(10.1, 20.1, 30.1));
		//overlaoding("saurav", "kumar", "sanghi");
		//String S=overlaoding("saurav", "kumar", "sanghi");
		System.out.println(overlaoding("saurav", "kumar", "sanghi"));
	}
   public static int overloading(int a,int b,int c){
	return(a+b+c);
}
   public static double overloading(double a,double b,double c){
		return(a+b+c);
   }
   public static String overlaoding(String a, String b,String c){
	   return(a+b+c);
   }
}


